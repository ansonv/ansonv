平安银行银企直连，需要自己部署Ｂ２Ｂｉ

安装说明：composer require ansonv/yii2-pingandirectconn

使用说明： 1.请在"config/params.php"中加入以下代码：

```php
'pinganBank'    => [
        'AccountNo' => '00901080000003333000',//此处填写企业银企直连标准代码（20位）
        'b2bic' => [
            'ip_address'    => '', //此处填写B2Bi前置机的IP地址
            'port'    => '7072', //此处是B2Bi前置机访问端口号，请确保没有防火墙拦截。
        ],
    ],
```

```php
use Ansonv\Yii2Pingandirectconn\PingAnBank;

class test(){
    $pingan = new PingAnBank();
    $result = $pingan->call($account_no,$api_id,$params);
    //$account_no：可以传空值，如果传空值即默认使用params.php中设置的AccountNo。
    //$api_id: 接口编号，查看文档，如4001，4002，JGP001
    //$params: 接口参数，数组形式
}
```

Ansonvvv - 2022-06-15