<?php

namespace Ansonv\Yii2Pingandirectconn\Lib;

use Yii;

/**
 * 拼接报文
 */
class Api extends Base
{

    public function __construct($accountNo)
    {
        $this->AccountNo = $accountNo;
        parent::__construct();
    }

    private function init()
    {

    }

    public function callApi($api_id, $params = [])
    {
        $context = $this->setApi($api_id)->pickupData($params);
        $res = $this->runSocket($context);
//        $result_xml = $this->extractXml($res);
//        $result_header = $this->extractHeader($res);
        return $this->unPackContext($res);
    }

}