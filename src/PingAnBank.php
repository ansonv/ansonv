<?php

namespace Ansonv\Yii2Pingandirectconn;

use Yii;

class PingAnBank
{
    private $api;
    private $accountNo;

    public function __construct()
    {
        $this->accountNo = Yii::$app->params['pinganBank']['AccountNo'];
        $this->api = Yii::createObject('Ansonv\Yii2Pingandirectconn\Lib\Api', [$this->accountNo]);
    }

    /**
     * 账户余额查询
     * @param $accountNo
     * @return array
     */
    public function getAccountInfo($accountNo)
    {
        //4001接口
        $params = [
            'Account' => $accountNo
        ];
        $result = $this->api->callApi('4001', $params);

        return $result;
    }


    /**
     * 平安易-交易明细查询[JGP009]
     * @param $payaccno 监管账号 -  必输 -
     * @param $startdate 起始日期 -  必输 -
     * @param $enddate 截止日期 -  必输 -
     * @param $trantype 交易类型 -  必输 -20-项目到账资金调整21-手动清分22-自动清分23-项目到账资金转待清分池30-项目交易明细31-账号交易明细32-项目转账33-待清分流水转账35-待清分流水转账原路退回
     * @param $payStatus 支付状态  S-成功 F-失败C-处理中
     * @param $PageNo 查询页码 -  非必输 -    第一页，依次递增
     * @param $PageSize 每页明细数量 - C(20) 1：默认每页30条记录，支持最大每页100条，若上送PageSize>100无效，等同100；
     * @param $projectName 项目名称 - C(20) 非必输 -
     */
    public function transaction_details($payaccno, $startdate, $enddate, $trantype, $payStatus, $PageNo = 1, $PageSize = 30, $projectName = '')
    {
        $params = [
            'Payaccno' => $payaccno,
            'startdate' => $startdate,
            'enddate' => $enddate,
            'trantype' => $trantype,
            'payStatus' => $payStatus,
            'PageNo' => $PageNo,
            'PageSize' => $PageSize,
            'projectName' => $projectName,
        ];
        $result = $this->api->callApi('JGP009', $params);
        return $result;
    }

    public function call($account_no = '',$api_id, $params)
    {
        if ($account_no != '') {
            return $this->api->setAccountNo($account_no)->callApi($api_id, $params);
        } else {
            return $this->api->callApi($api_id, $params);

        }
    }


}